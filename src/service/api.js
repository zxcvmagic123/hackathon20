import axios from "axios"

const instance = axios.create()

    instance.defaults.baseURL = "https://jsonplaceholder.typicode.com/"

    instance.defaults.headers ={
        "Content-Type":"application/json"
    };

    export default {
        onGetPosts(){
            return instance.get("posts")
        },
        onCreatePosts(body){
            return instance.post("posts",body)
        },
        onUpdatePosts(body){
            return instance.put("posts/"+body.id.body)
        },
        onDeletePosts(id){
            return instance.delete("posts/"+id)
        }

    };
